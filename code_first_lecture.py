import random


def read_avg_expression(file_name):
    avg_expression = {}
    file = open(file_name, "r")
    file_contents = file.readlines()
    file.close()
    for line in file_contents:
        key, value = line.split()
        avg_expression[key] = int(value)
    return avg_expression


def sample_transcripts(avgs, number):
    # we calculate the sum of all different copies
    s = sum(avgs.values())
    # The first dictionary will include all of the genes
    # and their propabilities (relative abunbancies)
    dict_prob = {}
    # The second dictionary will include the new sample we want to make
    dict_ret = {}
    # Επανάληψη για κάθε γραμμή που περιέχεται στο dictionary
    # που βάλαμε ως είσοδο στη συνάρτηση
    for item in avgs:
        # key <- gene_id
        key = item
        # dict[item] <- nr_of_copies
        # s <- 60
        # We calculate to make an percentage
        # and we round the number
        temp = int(dict[item])
        value = round((temp / s) * 100, 2)
        # for each gene_id, we put in the dictionary
        # its gene_id and its percentage
        dict_prob[key] = value
        # Additionally, we initialize the final sample we want to return
        # saying that all different genes are presented zero times
        dict_ret[key] = 0
    for i in range(number):
        # We magically choose each time a gene_id
        picked_key = random.choices(*zip(*dict_prob.items()))[0]
        # In the returned dictionary at the end of the function,
        # we add +1 to the gene which was selected
        dict_ret[picked_key] += 1
    return dict_ret


def write_sample(file, sample):
    file = open(file, "w")
    file.write(str(sample))


my_file = "directory_my_file.txt"
dict = read_avg_expression(my_file)
print(dict)
my_new_file = "directory_new_file.txt"